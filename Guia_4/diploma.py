#from estudiante import Estudiante
from asignatura import Asignatura

class Diploma():

    #Definimos los parámetros
    def __init__(self, estudiante, asignatura):
        #Declaramos los atributos (privados ocultos)
        self._estudiante = estudiante
        self._asignatura = asignatura

    @property
    #Definimos el método para obtener el nombre
    def estudiante(self):
        #estamos retornando el atributo privado oculto
        return self._estudiante
    
    @estudiante.setter
    #Definimos el metodo para entregar o dar el nombre del estudiante en el atributo privado
    def estudiante(self, nombre):
        # Condición si el parametro es de Tipo Estudiante
        if isinstance(nombre, Estudiante):
            self._estudiante = nombre
        else:
            print("no pertenece a clase adecuada diploma")
    
    @property
    #Definimos el método para obtener el nombre
    def asignatura(self):
        #estamos retornando el atributo privado oculto
        return self._asignatura
    
    @asignatura.setter
    #Definimos el metodo para entregar o dar la asignatura en el atributo privado
    def asignatura(self,asignatura):
        # Condición si el parametro es de Tipo Asignatura
        if isinstance(asignatura, Asignatura):
            self._asignatura = asignatura
        else:
            print("no pertenece a clase adecuada")