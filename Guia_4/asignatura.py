class Asignatura():

    #Definimos los parámetros
    def __init__(self, nombre):
        #Declaramos los atributos (privados ocultos)
        self._nombre = nombre


    @property
    #Definimos el método para obtener el nombre
    def nombre(self):
        #estamos retornando el atributo privado oculto
        return self._nombre