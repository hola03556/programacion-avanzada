from asignatura import Asignatura
from diploma import Diploma

class Estudiante():

    #Definimos los parámetros
    def __init__(self, nombre):
        #Declaramos los atributos (privados ocultos)
        self._nombre = nombre
        self._asignatura = []
        self._diploma = []

    @property
    #Definimos el método para obtener el nombre
    def nombre(self):
        #estamos retornando el atributo privado oculto
        return self._nombre

    @nombre.setter
    #Definimos el metodo para entregar o dar el nombre en el atributo privado
    def nombre(self, nombre):
        # Condición si el parametro es de Tipo String
        if isinstance(nombre, str):
            self._nombre = nombre
        else:
            print("El tipo de dato no corresponde")

    @property
    #Definimos el método para obtener asignatura
    def asignatura(self):
        #estamos retornando el atributo privado oculto
        return self._asignatura

    @asignatura.setter
    #Definimos el metodo para entregar o dar la asignatura en el atributo privado
    def asignatura(self, asignatura):
        # Condición si el parametro es de Tipo Asignatura
        if isinstance(asignatura, Asignatura):
            self._asignatura.append(asignatura)
        else:
            print("El tipo de dato no corresponde")
    
    @property
    #Definimos el método para obtener el diploma
    def diploma(self):
        #estamos retornando el atributo privado oculto
        return self._diploma

    @diploma.setter
    #Definimos el metodo para entregar o dar el diploma en el atributo privado
    def diploma(self, diploma):
        # Condición si el parametro es de Tipo Diploma
        if isinstance(diploma, Diploma):
            self._diploma.append(diploma)
        else:
            print("El tipo de dato no corresponde")

    @property
    #Definimos el método para obtener el diploma
    def diploma_remover(self):
        #estamos retornando el atributo privado oculto
        return self._diploma

    @diploma_remover.setter
    #Definimos el metodo para entregar o dar el diploma en el atributo privado
    def diploma_remover(self, diploma):
        # Condición si el parametro es de Tipo Diploma
        if isinstance(diploma, Diploma):
            self._diploma.remove(diploma)
        else:
            print("El tipo de dato no corresponde")