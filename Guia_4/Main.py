import random
from estudiante import Estudiante
from asignatura import Asignatura
from diploma import Diploma


def main():
    # Objetos diplomas, estudiantes y cursos

    # Objetos Asignatura
    Programacion = Asignatura("Programacion")
    Fisica = Asignatura("Fisica")
    Mineria = Asignatura("Mineria de Datos")
    Bioinformatica = Asignatura("Bioinformatica Estructural")

    # Lista de Objrtos Estudiante
    lista_estudiante = [Estudiante("Pablito"), Estudiante("Sebastian"), Estudiante("Matias"), Estudiante("Renata"), Estudiante("Denisse"), Estudiante("Raimundo"),
            Estudiante("Bastian"), Estudiante("Jose"), Estudiante("Diego"), Estudiante("Claudio"), Estudiante("Josefa"), Estudiante("Thomas")]

    # Lista de Objetos Asignaturas
    lista_asignatura = [Programacion, Fisica, Mineria, Bioinformatica]



    asignatura_perteneciente = []
    # Ciclo que asigna o le da más de una asignatura a cada Estudiante
    for i in lista_estudiante:
        x = random.randint(1, 4)
        contador = 0
        # Ciclo que saca de forma aleatoria las asignaturas del Estudiante, de manera que no se repita.
        while contador < x:
            temp = random.choice(lista_asignatura)
            # Condición que permite que no se le agrega una asignatura anteriormente dada.
            if temp not in asignatura_perteneciente:
                i.asignatura = temp
                asignatura_perteneciente.append(temp)
                contador += 1
        asignatura_perteneciente = []

    diplomas = []
    # Ciclo que agrega objetos tipo Diploma con el nombre del estudiante y la asignatura correspondiente en una lista
    for i in lista_estudiante:
        for j in i.asignatura:
            diplomas.append(Diploma(i, j))

    diplomas_pasados = []
    # Se ordenan de forma aleatoria los diplomas de la lista anterior , en una nueva lista 
    # y se le va removiendo el diploma sacado para que no se repita
    for did in range(len(diplomas)):
        x = random.choice(diplomas)
        diplomas.remove(x)
        diplomas_pasados.append(x)

    print("\t#########################")
    print("\t#  Entrega de Diplomas  #")
    print("\t#########################\n\n\n")

    # Ciclo que va recorriendo cada estudiante de la lista
    for i in lista_estudiante:
        # Ciclo que le entrega la cantidad de diplomas de forma aleaotoria dependiendo de la cantidad de asignatura que le corresponde al Estudiante
        for x in range(0, len(i.asignatura)):
            d_entregado = random.choice(diplomas_pasados)
            i.diploma = d_entregado
            diplomas_pasados.remove(d_entregado)

    print("*---------------------------------------------------------------------------------------------*")
    # Ciclo que va recorriendo cada estudiante de la lista
    for i in lista_estudiante:
        print(i.nombre, "Tiene:")
        # Ciclo que va recorriendo los diplomas que tiene en su posesión.
        for l in i.diploma:
            print(f'El diploma de Estudiante: {l.estudiante.nombre} de mejor rendimiento Asignatura: {l.asignatura.nombre}')
        print("*---------------------------------------------------------------------------------------------*")
    print("\n\n\n")

    print("\t#############################")
    print("\t#  Intercambio de Diplomas  #")
    print("\t#############################\n\n\n")
    # Remover Diplomas a Estudiantes que no les pertenecen
    diplomas_equivocados = []

    # Ciclo que va recorriendo cada estudiante de la lista
    for i in lista_estudiante:
        F = i.diploma
        vacia = []
        # Ciclo que va recorriendo los diplomas dados de forma aleatoria al estudiante
        for j in F:
            # Condición que comprueba si el objecto Estudiante es igual al nombre del estudiante que esta el diploma
            if i != j.estudiante:
                # Se agrega una lista los diplomas que no le pertenezca
                diplomas_equivocados.append(j)
                vacia.append(j)
        # Ciclo que recorre la lista que posee todos los diplomas que le pertenece
        for x in vacia:
            # Remueve el diploma equivocado de su posesión
            i.diploma_remover = x

    # Ciclo que recorre la lista de los diplomas equivocados
    for t in diplomas_equivocados:
        # Ciclo que recorre cada estudiante de la lista
        for i in lista_estudiante:
            # Condicion en que si el nombre del estudiante que está en diploma, es el mismo del Objecto Estudiante se lo entrega
            if t.estudiante.nombre == i.nombre:
                i.diploma = t
                break

    # Imprime en pantalla los Diplomas de cada Estudiante
    print("*---------------------------------------------------------------------------------------------*")
    # Ciclo que va recorriendo cada estudiante de la lista
    for i in lista_estudiante:
        print(i.nombre, "Tiene:")
        # Ciclo que va recorriendo los diplomas que tiene en su posesión.
        for l in i.diploma:
            print(f'El diploma de Estudiante: {l.estudiante.nombre} de mejor rendimiento Asignatura: {l.asignatura.nombre}')
        print("*---------------------------------------------------------------------------------------------*")


if __name__ == '__main__':
    main()
