import random
from alien import Alien
import armas

class Juego():

    def __init__(self):
        self._total_aliens_created = []
        self._alien_start_position = []
        pass

    @property
    def total_aliens_created(self):
        return self._total_aliens_created
    
    @total_aliens_created.setter
    def total_aliens_created(self, aliens):
        if isinstance (aliens, Alien):
            self._total_aliens_created.append(aliens)
            return len(self._total_aliens_created)
        else:
            print ("No es el tipo de Clase")

    @property
    def alien_start_position(self):
        return self._alien_start_position

    @alien_start_position.setter
    def alien_start_position(self, aliens):
        if isinstance (aliens, Alien):
            self._alien_start_position.append(Alien.x_coordinate, Alien.y_coordinate)
        else:
            print ("No es el tipo de Clase")

    def entregar_arma(self):
        i = 0
        for alien in self._total_aliens_created:
            eleccion = random.randint(0,3)
            if eleccion == 0:
                armita = armas.Mele()
            elif eleccion == 1:
                armita = armas.PistolaLaser()
            elif eleccion == 2:
                armita = armas.CanonProtones()
            elif eleccion == 3:
                armita = armas.GranadaAntimateria()
            alien.arma = armita
    
    def collision_detection(self, alien_1, alien_2):
        if isinstance(alien_1, Alien) and isinstance (alien_2, Alien):
            for alien_obj in (alien_1, alien_2):
                alien_obj.hit()

        else:
            return False