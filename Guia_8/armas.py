class Arma():
    def __init__(self):
        self._nombre = None
        self._danodado = None
        
    @property
    def nombre(self):
        return self._nombre
    
    @property
    def danodado(self):
        return self._danodado

class Mele(Arma):
    def __init__(self):
        #Declaramos los atributos (privados ocultos)
        super().__init__()
        self._nombre = "Mele"
        self._danodado = 0
class PistolaLaser(Arma):
    def __init__(self):
        #Declaramos los atributos (privados ocultos)
        super().__init__()
        self._nombre = "Pistola Láser"
        self._danodado = 1

#Clase Hija Trebol de Clase Plant
class CanonProtones(Arma):
    def __init__(self):
        #Declaramos los atributos (privados ocultos)
        super().__init__()
        self._nombre = "Cañon de Protones"
        self._danodado = 1

#Clase Hija Rabano de Clase Plant
class GranadaAntimateria(Arma):
    def __init__(self):
        #Declaramos los atributos (privados ocultos)
        super().__init__()
        self._nombre = "Granada de Antimateria"
        self._danodado = 1

