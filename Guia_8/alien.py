from armas import Arma


class Alien ():
    def __init__(self, x, y , nombre):
        self._nombre = nombre
        self._x_coordinate = x
        self._y_coordinate = y
        self._health = 3
        self._arma = None

    @property
    def nombre(self):
        return self._nombre

    @nombre.setter
    def nombre(self,nombre):
        if isinstance(nombre,str):
            self._nombre = nombre
    
    @property
    def x_coordinate(self):
        return self._x_coordinate
    
    @x_coordinate.setter
    def x_coordinate(self, x):
        if isinstance (x, int):
            if x >= 0 and x <= 10:
                self._x_coordinate = self._x_coordinate
            else:
                print("Esta fuera del Escenario en X (0 hasta el 10)")
        else:
            print("No es el tipo de Clase")

    @property
    def y_coordinate(self):
        return self._y_coordinate
    
    @y_coordinate.setter
    def y_coordinate(self, y):
        if isinstance (y, int):
            if y >= 0 and y <= 10:
                self._y_coordinate = self._y_coordinate
            else:
                print("Esta fuera del Escenario en Y (0 hasta el 10)")
        else:
            print("No es el tipo de Clase")

    @property
    def health(self):
        return self._health

    def hit(self, comprobar,turno,alien_1,alien_2):
        arma_posesion = self._arma
        if comprobar == 1 and turno == 0:
            danio = arma_posesion.danodado
            salud = self._health - danio
            alien_2.health = salud
        if comprobar == 1 and turno == 1:
            danio = arma_posesion.danodado
            salud = self._health - danio
            alien_1.health = salud

    
    def is_alive(self):
        if self._health != 0:
            return True
        else:
            return False

    def teleport(self, x, y):
        if isinstance (x, int) and isinstance (y, int):
            if (x >= 0 and x <= 10) and (y >= 0 and y <= 10):
                self._x_coordinate = x
                self._y_coordinate = y
            else:
                print("Esta fuera del Escenario en X, Y (0,0 hasta el 10,10)")
        else:
            print("No es el tipo de Clase")

    @property
    def arma(self):
        return self._arma

    @arma.setter
    def arma(self,arma):
        if isinstance(arma,Arma):
            self._arma = arma
