from Juego import Juego
from alien import Alien
from armas import Arma
import random

def aliens_elegidos(lista):
    alien_B = 0
    while True:
        alien_A = random.choice(lista)
        if (alien_A != alien_B) and (alien_B != 0):
            return alien_A,alien_B
        alien_B = alien_A  

def disparo (cord1, cord2, cord3, cord4, direccion, turno):
    #NORTE
    
    if direccion == 1:
        x = cord1
        while x < 10:
            x = cord1 + 1
            if cord3 == x:
                return 1
        return 0         
    #OESTE
    elif direccion == 2:
        x = cord1
        y = cord2
        while x < 10 or y < 10:
            x = cord1 + 1
            y = cord2 + 1
            if cord3 == x and cord4 == y:
                return 1
        return 0
    #SUR
    elif direccion == 3:
        x = cord1
        y = cord2
        while y < 0:
            y = cord2 - 1
            if cord2 == cord4:
                return 1
        return 0
    #ESTE
    elif direccion == 4:    
        x = cord1
        y = cord2    
        while x > 0 or y > 0:
            x = cord1 - 1
            y = cord2 - 1
            if cord3 == x and cord4 == y:
                return 1
        return 0

def main():
    
    escenario = Juego()
    cantidad_alien = 3
    #random.randint(2,6)

    for num in range(cantidad_alien):
        x = random.randint(0,10)
        y = random.randint(0,10)
        nombre = f'Alien_{num+1}'
        #if contador == 0:
        escenario.total_aliens_created = Alien(x,y,nombre)

    escenario.entregar_arma()
    lista_aliens = escenario.total_aliens_created  
    i = 0
    while True:
        turno = 0
        
        if i == 0:
            alien_1, alien_2 = aliens_elegidos(lista_aliens)
            i = 1
        if turno%2 == 0:
            turno_presente = turno%2

            print("Turno del Alien_1")
            print("Elija una accion:")
            print("<1> Teleportarse")
            print("<2> Disparar")
            seleccion = random.randint(1,2)
            print(f'Selecciono el <{seleccion}>')

            if seleccion == 1:
                x = random.randint (0,10)
                y = random.randint (0,10)
                alien_1.teleport(x, y)

            elif seleccion == 2:
                tiro = random.randint (1,4)
                cordx_alien1 = alien_1.x_coordinate 
                cordy_alien1 = alien_1.y_coordinate
                cordx_alien2 = alien_2.x_coordinate
                cordy_alien2 = alien_2.x_coordinate
                comprobacion = disparo(cordx_alien1, cordy_alien1, cordx_alien2, cordy_alien2, tiro,turno,)
                alien_1.hit(comprobacion, turno, alien_1,alien_2)
                vivo = alien_2.is_alive
                if vivo:
                    print(f'La vida del Alien 2 es {alien_2.health}')
                else:
                    print(f'La vida del Alien 2 es {alien_2.health}')
                    i = 0
            turno = turno + 1

        else:
            turno = 1
            print("Turno del Alien_2")
            print("Elija una accion:")
            print("<1> Teleportarse")
            print("<2> Disparar")
            seleccion = random.randint(1,2)
            print(f'Selecciono el <{seleccion}>')

            if seleccion == 1:
                x = random.randint (0,10)
                y = random.randint (0,10)
                alien_1.teleport(x, y)

            elif seleccion == 2:
                print("Seleccione una direccion para disparar")
                input
                tiro = random.randint (1,4)
                cordx_alien1 = alien_1.x_coordinate 
                cordy_alien1 = alien_1.y_coordinate
                cordx_alien2 = alien_2.x_coordinate
                cordy_alien2 = alien_2.x_coordinate
                comprobacion = disparo(cordx_alien2, cordy_alien2, cordx_alien1, cordy_alien1, tiro,turno)
                alien_2.hit(comprobacion, turno, alien_1,alien_2)
                vivo = alien_1.is_alive
                if vivo:
                    print(f'La vida del Alien 1 es {alien_1.health}')
                else:
                    print(f'La vida del Alien 2 es {alien_1.health}')
                    i = 0

            turno = turno + 1


if __name__ == "__main__":
    main()
