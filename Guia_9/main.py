from fighter import Fighter
from Warrior import warrior
from Wizard import wizard
from time import sleep

"""
No hubo sobrecarga en el trabajo de la guia
de hechiceros y guerreros, porque la manera de operar los metodos y clases 
del codigo de era necesario su implementacion
"""


def danio(TodoONada_2,guerrero,hechicero):
    # Condicion que depende de la vulnerabilidad o invulnerabilidad del hechicero
    # Que depende del hechizo del mago y los daños de cada uno

    if TodoONada_2:#Vulnerable
        danio_guerrero = 10
        danio_hechicero = 3
        vida_guerrero = guerrero.health
        vida_hechicero = hechicero.health
        vida_hechicero = vida_hechicero - danio_guerrero
        vida_guerrero = vida_guerrero - danio_hechicero

    else:#Invulnerable
        danio_guerrero = 6
        danio_hechicero = 12
        vida_guerrero = guerrero.health
        vida_hechicero = hechicero.health
        vida_hechicero = vida_hechicero - danio_guerrero
        vida_guerrero = vida_guerrero - danio_hechicero

    guerrero.health = vida_guerrero
    hechicero.health = vida_hechicero


# Funcion que entrega toString de cada objeto
# Que seria el polimorfismo
def toString(clase_guerrero,clase_mago):
    for obj in (clase_guerrero,clase_mago):
        print(obj.toString)



def main():
    # Creacion de tipos de Objetos
    guerrero = warrior()
    hechicero = wizard()
    toString(guerrero,hechicero)

    # Ciclo que recrea el escenario de pelea
    while guerrero.health > 0 and hechicero.health > 0:
        print("*"*30)
        print(f'--< El {guerrero.nombre} >--')
        print(f'### El {guerrero.nombre} esta preparando su ataque')
        print(f'### El {guerrero.nombre} tiene Vulnerabilidad: {guerrero.isVulnerable}')
        print("Ahora es Vulnerable")
        print("- "*30)
        print(f'--< El {hechicero.nombre} >--')
        print(f'### El {hechicero.nombre} esta preparando su ataque')
        ataque_2 = hechicero.prepareSpell()
        # Condicion si la preparacion del hechizo del hechicero 
        if ataque_2:
            # si el hechizo esta cargado , el hechicero se vuelve invulnerable
            TodoONada_2 = hechicero.isVulnerable
            print("### Su hechizo esta cargado ###")
            print(f'### El {hechicero.nombre} tiene Vulnerabilidad: {hechicero.isVulnerable}')
            print("### Ahora es Invulnerable ###")
        else:
            # Cuando el hechizo del hechicero no esta cargado, el hechicero es vulnerable
            # Se le cambia la vulnerabilidad del hechicero en la siguiente ronda
            hechicero.Vulnerable()
            TodoONada_2 = hechicero.isVulnerable
            print("Su hechizo no lo ha cargado cargado")
            print(f'El {hechicero.nombre} tiene Vulnerabilidad: {hechicero.isVulnerable}')
            print("Ahora es Vulnerable")
        # Funcion que da el daño del hechicero y del guerrero dependiendo del estado del hechicero
        danio(TodoONada_2,guerrero,hechicero)
        if guerrero.health > 0 and  hechicero.health > 0:
            print(f'### La vida del {guerrero.nombre} es [{guerrero.health}]')
            print(f'### La vida del {hechicero.nombre} es [{hechicero.health}]')
        sleep(0.5)
        
    print("*"*30)   
    if guerrero.health > hechicero.health and hechicero.health < 0:
        hechicero.health = 0
        print(f'El ganador de la pelea es el {guerrero.nombre}')
        print(f'La vida del {guerrero.nombre} es [{guerrero.health}]')
        print(f'La vida del {hechicero.nombre} es [{hechicero.health}]')
    elif guerrero.health < hechicero.health and guerrero.health < 0:
        guerrero.health = 0
        print(f'El ganador de la pelea es el {hechicero.nombre}')
        print(f'La vida del {guerrero.nombre} es [{guerrero.health}]')
        print(f'La vida del {hechicero.nombre} es [{hechicero.health}]')
    else:
        guerrero.health = 0
        hechicero.health = 0
        print(f'El ganador de la pelea es el {hechicero.nombre}')
        print(f'La vida del {guerrero.nombre} es [{guerrero.health}]')
        print(f'La vida del {hechicero.nombre} es [{hechicero.health}]')



if __name__ == "__main__":
    main()
