from fighter import Fighter
import random

class warrior(Fighter):
    def __init__(self):
        self._toString = "Fighter es un Warrior"
        self._nombre = "Vikingo*Berserker"
        self._isVulnerable = False
        self._health = 100

    @property
    def nombre(self):
        return self._nombre

    @property
    def toString(self):
        return self._toString

    @property
    def isVulnerable(self):
        return self._isVulnerable


    @property
    def health(self):
        return self._health

    @health.setter
    def health(self,vida):
        if isinstance(vida,int):
            self._health = vida
