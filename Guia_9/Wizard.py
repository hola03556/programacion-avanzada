from fighter import Fighter
import random 

class wizard(Fighter):#,abstra_Wizard):
    def __init__(self):
        super().__init__()
        self._toString = "Fighter es un Wizard"
        self._nombre = "Maguito*Explosivo"
        self._isVulnerable = False
        self._health = 100

    @property
    def nombre(self):
        return self._nombre
    
    @property
    def toString(self):
        return self._toString

    def prepareSpell(self):
        x = random.randint(0,1)
        if x == 1:
            self._prepareSpell = True
        elif x == 0:
            self._prepareSpell = False
        return self._prepareSpell

    @property
    def isVulnerable(self):
        return self._isVulnerable

    def Vulnerable(self):
        self._isVulnerable = not self._isVulnerable

    @property
    def health(self):
        return self._health

    @health.setter
    def health(self,vida):
        if isinstance(vida,int):
            self._health = vida
