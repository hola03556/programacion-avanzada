from abc import ABC
from abc import  abstractmethod

class Fighter(ABC):
    def __init__(self):
        self._toString = None

    @property
    @abstractmethod
    def toString(self):
        return self._toString

    @abstractmethod
    def isVulnerable(self):
        pass

    
    @abstractmethod
    def health(self):
        pass
