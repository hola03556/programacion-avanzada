from ciudadano import Ciudadano

# Comunidad
class Comunidad():
    # Clase del objeto Comunidad
    def __init__(self, num_ciudadanos, promedio_conexion_fisica, enfermedad, num_infectados, probabilidad_conexion_fisica):
        self._num_ciudadanos = num_ciudadanos
        self._promedio_conexion_fisica = promedio_conexion_fisica
        self._enfermedad = enfermedad
        self._num_infectados = num_infectados
        self._probabilidad_conexion_fisica = probabilidad_conexion_fisica
        self._comunidad_ciudadanos = []


    @property 
    def num_cuidadanos(self):
        return self._num_ciudadanos

    @property
    def promedio_conexion_fisica(self):
        return self._probabilidad_conexion_fisica

    @property
    def enfermedad(self):
        return self._enfermedad

    @property
    def num_infectados(self):
        return self._num_infectados

    @property
    def probabilidad_conexion_fisica(self):
        return self._probabilidad_conexion_fisica

    @property
    def comunidad_ciudadanos(self):
        return self._comunidad_ciudadanos

    @comunidad_ciudadanos.setter
    def comunidad_ciudadanos(self, ciudadano):
        if isinstance(ciudadano, Ciudadano):
            self._comunidad_ciudadanos.append(ciudadano)

    def crear_comunidad(self, ciudad):
            total = self._num_ciudadanos + 1
            # Individuos
            for x in range(1, total):
                Persona = Ciudadano(ciudad)
                Persona.id_ciudadano = str(x)
                # Agregar Ciudadanos en la comunidad
                self._comunidad_ciudadanos.append(Persona)
            for integrante in self._comunidad_ciudadanos:
                integrante.amigos()