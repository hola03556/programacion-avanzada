class Enfermedad():
    # Clase del objeto Enfermedad
    def __init__(self, infeccion_problable, promedio_pasos):
        self._infeccion_problable = infeccion_problable
        self._promedio_pasos = promedio_pasos
        self._contador = 0

    # Probabilidad de contagiarse debido a contacto fisico
    @property
    def infeccion_problable(self):
        return self._infeccion_problable

    # Cantidad de dias que tarda en recuperarse
    @property
    def promedio_pasos(self):
        return self._promedio_pasos
    

    @property
    def contador(self):
        return self._contador 

