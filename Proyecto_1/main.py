# Se importan los modulos necesarios
from enfermedad import Enfermedad
from comunidad import Comunidad
from simulador import Simulador

# Se crea la enfermedad
covid = Enfermedad (infeccion_problable = 3, promedio_pasos = 18)
# Se crea la comunidad
talca = Comunidad(num_ciudadanos = 20000, promedio_conexion_fisica = 8,
                    enfermedad = covid,num_infectados = 10,
                    probabilidad_conexion_fisica = 0.8)
# Se crea el simulador 
sim = Simulador(talca)
# Se crea los ciudadanos de la comunidad
talca.crear_comunidad(talca)
# Se crea los infectados en la comunidad
sim.set_comunidad()
# Se ejecuta la sumulación
sim.run ( pasos = 75)