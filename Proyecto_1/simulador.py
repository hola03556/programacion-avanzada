import pickletools
from comunidad import Comunidad
from ciudadano import Ciudadano
from enfermedad import Enfermedad
import random as rd
from time import sleep


class Simulador():
    def __init__(self, comunidad):
        self._comunidad = comunidad
        self._infectados = []
        self._susceptibles = []
        self._recuperados = []
        self._pasos = 0

    # Crear el Objeto Comunidad 
    def set_comunidad(self):
        # Infectados
        infectados = rd.sample(self._comunidad._comunidad_ciudadanos, self._comunidad._num_infectados)
        for inf in infectados:
            inf.enfermedad = self._comunidad.enfermedad
            inf.dias_cuarentena = self._comunidad.enfermedad.promedio_pasos
            inf.condicion()

    def run(self, pasos):
        recuperados  = 0
        peak = 0
        print("------------------------------------------------------------------")
        print(f'Dia *0*: Susceptibles {len(self._comunidad._comunidad_ciudadanos)} - Recuperados 0 - Infectados {self._comunidad._num_infectados}')
        # Inicia la simulucion por dias 
        for dias in range(pasos):
            infectados_actuales = 0
            print("------------------------------------------------------------------")
            # Se recorre cada persona de la comunidad
            for persona in self._comunidad._comunidad_ciudadanos:
                # Si la persona esta sana pasa a la siguiente persona
                if not persona.estado:
                    # Si la persona está contagiada, se le va quitando dias de su cuarentena
                    persona.dia_siguiente()
                    # Si la persona completa su cuarentena, se recupera y ya no es capaz de contagiar a más personas
                    # Pasa a la siguiente persona 
                    if persona.inmunidad:
                        persona.condicion()
                        # Se suma al total de recuperados
                        recuperados += 1
                        continue
                    # Se acumula las personas contagiadas del dia o actualmente
                    infectados_actuales += 1
                    # Se ve la probabilidad si hay reunión con una persona o no
                    reunion = rd.randint(0,100)
                    if reunion <= self._comunidad.probabilidad_conexion_fisica * 100:
                        # Si hay reunión, se escoge una persona aleatoria de su grupo de amigos
                        encuentro = rd.choice(persona.grupo)
                        # Si la persona escogida esta sana y sin inmunidad a la enfermedad presente
                        if encuentro.estado and not encuentro.inmunidad:
                            # probabilidad si se contagie de la enfermedad
                            contagio = rd.randint(0,100)
                            # La persona esta por contagiarse
                            if contagio <= self._comunidad.enfermedad.infeccion_problable * 100:
                                # La persona se contagia con la enfermedad
                                encuentro.enfermedad = self._comunidad.enfermedad
                                # SE le ingresa la cantidad de dias que tiene que hacer cuarentena
                                encuentro.dias_cuarentena = self._comunidad.enfermedad.promedio_pasos
                                # La persona cambia de estado no sano
                                encuentro.condicion()
            if peak == 0 or peak < infectados_actuales:
                peak = infectados_actuales
                dia = dias
            # Total de susceptibles dependiente a la cantidad de recuperados y infectados
            susceptibles = len(self._comunidad._comunidad_ciudadanos) - recuperados - infectados_actuales
            # Reporte de susceptibles, infectados y recuperados por cada dia
            print(f'Dia *{dias + 1}*: Susceptibles {susceptibles} - Recuperados {recuperados} - Infectados {infectados_actuales}')
            sleep(0.5)
        print("\n\n\n##########################################################")
        print(f'# El peak de infectados fue de {peak} -- El dia {dia+1} #')
        print("##########################################################")

