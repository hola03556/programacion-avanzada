from enfermedad import Enfermedad
import random as rd

class Ciudadano():
    # Clase del objeto Ciudadano 
    def __init__(self, comunidad):
        self._comunidad = comunidad
        self._id_ciudadano = ''
        self._grupo= []
        self._enfermedad = None
        self._estado = True
        self._inmunidad = False
        self._dias_cuarentena = None

    @property
    def comunidad(self):
        return self._comunidad

    @property
    def id_ciudadano(self):
        return self._id_ciudadano

    @id_ciudadano.setter
    def id_ciudadano(self, codigo):
        if isinstance(codigo,str):
            # Se crea el ID unico de cada ciudadano
            cantidad_total = str(self._comunidad._num_ciudadanos)
            if len(cantidad_total) > len(codigo):
                texto_id = '0'*(len(cantidad_total)-len(codigo))
                codigo_texto = f'{texto_id}{codigo}'
                self._id_ciudadano = codigo_texto
            else:
                codigo_texto = f'{codigo}'
                self._id_ciudadano = codigo_texto

    @property
    def grupo(self):
        return self._grupo

    def amigos(self):
        # Se crea el grupo de amigos de cada ciudadano 
        while len(self._grupo) < self.comunidad._promedio_conexion_fisica:
            amigo = rd.choice(self._comunidad.comunidad_ciudadanos)
            # Sin que se repita un amigo o salgo uno mismo
            if (amigo not in self._grupo) and (amigo._id_ciudadano != self._id_ciudadano):
                self._grupo.append(amigo)

    @property
    def enfermedad(self):
        return self._enfermedad
    
    @enfermedad.setter
    def enfermedad(self,virus):
        # Se le asigna la enfermedad a una persona
        if isinstance(virus,Enfermedad):
            self._enfermedad = virus

    @property
    def estado(self):
        return self._estado
    
    def condicion(self):
        # Si esta sano o infectado el ciudadano
        self._estado = not self._estado

    @property
    def dias_cuarentena(self):
        return self._dias_cuarentena

    @dias_cuarentena.setter
    def dias_cuarentena(self, pasos):
        # Los dias de cuarentena que debe cumplir si se infecta un ciudadano
        if isinstance(pasos,int):
            self._dias_cuarentena = pasos
    
    def dia_siguiente(self):
        # Se le resta un dia a la cuarentena hasta que termine 
        if self._dias_cuarentena > 0:
            self._dias_cuarentena -= 1
        # Cuando termina la cuarentena, el ciudadano esta sano o muerto, sin que se vuelva a contagiar
        else:
            self._inmunidad = True

    @property
    def inmunidad(self):
        return self._inmunidad
