
class Plant():
    def __init__(self):
        #Declaramos los atributos (privados ocultos)
        self._nombre = None

    @property
    #Definimos el método para obtener el nombre
    def nombre(self):
        #estamos retornando el atributo privado oculto
        return self._nombre

#Clase Hija Violeta de Clase Plant
class Violeta(Plant):
    def __init__(self):
        #Declaramos los atributos (privados ocultos)
        super().__init__()
        self._nombre = "Violeta"

#Clase Hija Hierba de Clase Plant
class Hierba(Plant):
    def __init__(self):
        #Declaramos los atributos (privados ocultos)
        super().__init__()
        self._nombre = "Hierba"

#Clase Hija Trebol de Clase Plant
class Trebol(Plant):
    def __init__(self):
        #Declaramos los atributos (privados ocultos)
        super().__init__()
        self._nombre = "Trebol"

#Clase Hija Rabano de Clase Plant
class Rabano(Plant):
    def __init__(self):
        #Declaramos los atributos (privados ocultos)
        super().__init__()
        self._nombre = "Rabano"

