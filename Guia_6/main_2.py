from alumno import Alumno
from garden import Garden
import plant


# Muesta en Pantalla todas las plantas de los estudiantes en la ventanas
def ventana_plantas(jardin):
    print("\n")
    print("*"*63)
    print("[ventana]"*7)
    Fila_1 = " "
    Fila_2 = " "
    # Ciclo for que recorre cada Objeto tipo Alumno de la lista de la clase Garden
    for child in jardin.alumnos:
        # Variable que almacena las plantas de cada Alumno
        plantitas = child.vasitos
        # Ciclo que recorre  las 2 primeras plantas del alumno y se agrega su inicial en un string
        for item in range(0,2):
            Fila_1 += plantitas[item].nombre[0]
        if child != jardin.alumnos[-1]:
            Fila_1 += " - "
        # Ciclo que recorre  las 2 ultimas plantas del alumno y se agrega su inicial en un string
        for item_2 in range(2,4):
            Fila_2 += plantitas[item_2].nombre[0]
        if child != jardin.alumnos[-1]:
            Fila_2 += " - "
    print(" ",Fila_1)
    print(" ",Fila_2)
    print("*"*63)



# Muesta en Pantalla todas las plantas de los estudiantes en modo lista
def plantas_lista(jardin):
    print("\n")
    print("*"*80)
    # Ciclo for que recorre cada Objeto tipo Alumno de la lista de la clase Garden
    for child in jardin.alumnos:
        plantitas_en_posesion = []
        # Agrega las plantas del alumno correspondiente
        for plantas in jardin.plantas(child):
            plantitas_en_posesion.append(plantas)
        # Imprime el alumno y los nombres de sus cuatros plantas en pantalla
        print(child.nombre,": ", plantitas_en_posesion[0].nombre," - ",plantitas_en_posesion[1].nombre," - ", plantitas_en_posesion[2].nombre," - ",plantitas_en_posesion[3].nombre)
    print("*"*80)



# Muesta en Pantalla todas las plantas de los estudiantes en modo abreviada
def plantas_resumen(jardin):
    print("\n")
    print("*"*80)
    # Ciclo for que recorre cada Objeto tipo Alumno de la lista de la clase Garden
    for child in jardin.alumnos:
        plantitas_en_posesion = []
        # Agrega las plantas del alumno correspondiente
        for plantas in jardin.plantas(child):
            plantitas_en_posesion.append(plantas)
        # Imprime el alumno y las primeras letras de sus cuatros plantas en pantalla
        print(child.nombre,": ", plantitas_en_posesion[0].nombre[0]," - ",plantitas_en_posesion[1].nombre[0]," - ", plantitas_en_posesion[2].nombre[0]," - ",plantitas_en_posesion[3].nombre[0])
    print("*"*80)


def main():

    # Objeto tipo Garden
    jardin = Garden()

    #Lista de alumnos que se ordena por orden alfabetico
    nombres_alumnos = ["Alicia", "Marit", "Pepito", "David","Eva","Lucia","Rocio","Andres","Jose","Belen","Sergio","Larry"]
    nombres_alumnos.sort()

    # Ciclo que crea y agrega en una lista objetos tipo Alumno
    for i in nombres_alumnos:
        jardin.alumnos = Alumno(i)
    jardin.entregar_vasitos()


    while True:
        # Menu de Interacción de Jardin Media Luna
        print("*"*90)
        print("\n\n\t**** Bienvenido al Jardin Media Luna ****\n\n")
        print("<1> Todas plantas de los alumnos en la ventana")
        print("<2> PLantas de un alumno en especifico en la ventana")
        print("<3> Plantas de cada alumno en forma de lista")
        print("<4> Plantas de cada alumno en forma resumen")
        print("<0> Finalizar ejecución")
        opcion = int(input("Escoja una opcion --> "))
        if opcion == 1:
            # Llamado de la funcion Ventana_plantas
            # Se le entrega como parametro Objeto tipo Garden
            ventana_plantas(jardin)
        elif opcion == 2:
            i = 1
            print("\n\n\n**********************")
            # Ciclo que muestra el menu de los alumnos del jardin
            for child in jardin.alumnos:
                print(f'<{i}>',child.nombre)
                i +=1
            print("**********************")
            indice = int(input("# Ingrese el numero del alumno que desea ver sus plantitas: "))
            # LLamado del metodo ventana_plantita_Alumno de objeto tipo Garden
            Fila_1,Fila_2 = jardin.ventana_plantita_Alumno(jardin.alumnos[indice-1])
            print(Fila_1)
            print(Fila_2)
            print("*"*60)
        elif opcion == 3:
            # LLamado de la funcion plantas_lista y se le entrega como parametro objeto tipo Garden
            plantas_lista(jardin)
        elif opcion == 4:
            # # LLamado de la funcion plantas_resumen y se le entrega como parametro objeto tipo Garden
            plantas_resumen(jardin)
        elif opcion == 0:
            break
        else:
            print("*"*46)
            print("* Opcion Invalida. Ingrese una Opcion Valida *")
            print("*"*46)


if __name__ == "__main__":
    main()