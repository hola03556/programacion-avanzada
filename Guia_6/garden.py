from alumno import Alumno
import plant
import random


class Garden():
    def __init__(self):
        #Declaramos los atributos (privados ocultos)
        self._alumnos = []

    @property
    #Definimos el método para obtener los alumnos
    def alumnos(self):
        #estamos retornando el atributo privado oculto
        return self._alumnos

    @alumnos.setter
    #Definimos el metodo para entregar o dar la lista alumnos en el atributo privado
    def alumnos(self,alumnos):
        # Condición si el parametro es de Tipo Alumno
        if isinstance(alumnos, Alumno):
            self._alumnos.append(alumnos)
        else:
            print("No es de este tipo de clase")
        
    @property
    #Definimos el método para obtener el nombre
    def nombre(self):
        #estamos retornando el atributo privado oculto
        return self._nombre

    @nombre.setter
    #Definimos el metodo para entregar o dar el nombre en el atributo privado
    def nombre(self,nombre):
        # Condición si el parametro es de Tipo String
        if isinstance(nombre,str):
            self._nombre = nombre
        else:
            print("NO es tipo de esta clase")

    # Definimos el metodo para asignar 4 plantas a cada estudiante
    def entregar_vasitos(self):
        i = 0
        # Ciclo que recorre las lista alumnos
        for x in self._alumnos:
            # Ciclo que entrega 4 plantas de forma aleatoria a cada alumno
            for y in range(4):
                eleccion = random.randint(1,4)
                if eleccion == 1:
                    semillita = plant.Rabano()
                    x.vasitos = semillita
                elif eleccion == 2:
                    semillita = plant.Trebol()
                    x.vasitos = semillita
                elif eleccion == 3:
                    semillita = plant.Violeta()
                    x.vasitos = semillita
                elif eleccion == 4:
                    semillita = plant.Hierba()
                    x.vasitos = semillita

    # Definimos el metodo para imprimir las letras iniciales del alumno elegido
    def ventana_plantita_Alumno(self, sujeto):
        # Condición si el parametro es de Tipo Alumno
        if isinstance(sujeto, Alumno):
            print("\n")
            print("*"*60)
            Fila_1 = " "
            Fila_2 = " "
            # Ciclo que recorre la lista Alumnos por cada uno
            for child in self._alumnos:
                # Se le asigna la lista de plantas del alumno en una variable
                plantitas = child.vasitos
                if sujeto.nombre == child.nombre:
                    # Ciclo que recorre  las 2 primeras plantas del alumno y se agrega su inicial en un string
                    for item in range(0,2):
                        # Ciclo que recorre  las 2 primeras plantas del alumno y se agrega su inicial en un string
                        Fila_1 += plantitas[item].nombre[0]
                if child != self._alumnos[-1]:
                    Fila_1 += " - - "
                if sujeto.nombre == child.nombre:
                    # Ciclo que recorre  las 2 ultimas plantas del alumno y se agrega su inicial en un string
                    for item_2 in range(2,4):
                        Fila_2 += plantitas[item_2].nombre[0]
                if child != self._alumnos[-1]:
                    Fila_2 += " - - "
        # Retorno de las variables filas
        return Fila_1,Fila_2


    # Definimos el metodo para retornar las plantas de cada alumno
    def plantas(self, alumno):
        # Condición si el parametro es de Tipo Alumno
        if isinstance(alumno,Alumno):
            lista = alumno.vasitos
            return lista
        else:
            print("NO es de tipo de la clase")