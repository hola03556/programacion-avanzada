import plant
class Alumno():
    def __init__(self, nombre):
        #Declaramos los atributos (privados ocultos)
        self._nombre = nombre
        self._vasitos = []

    @property
    #Definimos el método para obtener el nombre
    def nombre(self):
        #estamos retornando el atributo privado oculto
        return self._nombre

    @nombre.setter
    #Definimos el metodo para entregar o dar el nombre en el atributo privado
    def nombre(self, nombre):
        # Condición si el parametro es de Tipo String
        if isinstance(nombre, str):
            #estamos retornando el atributo privado oculto
            self._nombre = nombre
        else:
            print("NO es tipo de esta clase")
    
    @property
    #Definimos el método para obtener la lista vasitos
    def vasitos(self):
        #estamos retornando el atributo privado oculto
        return self._vasitos

    @vasitos.setter
    #Definimos el metodo para entregar o dar el un objeto tipo plant
    # a una lista ss
    def vasitos(self, plantita):
        # Condición si el parametro es de Tipo Plant
        if isinstance(plantita, plant.Plant):
            if len(self._vasitos) < 4:
                self._vasitos.append(plantita)
        else:
            print("No es tipo de esta clase")
