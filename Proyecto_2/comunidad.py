from ciudadano import Ciudadano
from enfermedad_base import Enfermedad_Base, Cerebro_vascular,Fibrosis_Quistica,Hipertension,Asma, Ninguna_Enfermedad
from afeccion import Afeccion, Obesidad, Desnutricion, Ninguna_Afeccion
import random as r
import vacuna

# Comunidad
class Comunidad():
    # Clase del objeto Comunidad
    def __init__(self, num_ciudadanos, promedio_conexion_fisica, enfermedad, num_infectados, probabilidad_conexion_fisica):
        self._num_ciudadanos = num_ciudadanos
        self._promedio_conexion_fisica = promedio_conexion_fisica
        self._enfermedad = enfermedad
        self._num_infectados = num_infectados
        self._probabilidad_conexion_fisica = probabilidad_conexion_fisica
        self._comunidad_ciudadanos = []
        self._enfermedades = [Cerebro_vascular(), Fibrosis_Quistica(), Hipertension(), Asma()] 
        self._afecciones = [Obesidad(), Desnutricion()]
        self._total_vacunas = self._num_ciudadanos/2
        self._total_vacunas_1 = round(self._num_ciudadanos/2)
        self._total_vacunas_2 = round(self._num_ciudadanos/3)
        self._total_vacunas_3 = round(self._num_ciudadanos/6)
        self._total_enfermos = 0
        self._total_afecciones = 0
        self._Promedio = None
        self._total_afecciones_y_enfermos = 0


    @property 
    def num_cuidadanos(self):
        return self._num_ciudadanos

    @property
    def promedio_conexion_fisica(self):
        return self._probabilidad_conexion_fisica

    @property
    def enfermedad(self):
        return self._enfermedad

    @property
    def num_infectados(self):
        return self._num_infectados

    @property
    def probabilidad_conexion_fisica(self):
        return self._probabilidad_conexion_fisica

    @property
    def comunidad_ciudadanos(self):
        return self._comunidad_ciudadanos

    @comunidad_ciudadanos.setter
    def comunidad_ciudadanos(self, ciudadano):
        if isinstance(ciudadano, Ciudadano):
            self._comunidad_ciudadanos.append(ciudadano)

    def crear_comunidad(self, ciudad):
            total = self._num_ciudadanos + 1
            sumatoria_edades = 0
            # Individuos
            for x in range(1, total):
                anio = r.randint(1,95)
                sumatoria_edades += anio
                Persona = Ciudadano(ciudad)
                Persona.id_ciudadano = str(x)
                # Edad Del Ciudadano
                Persona._edad = anio
                if Persona.edad <= 15:
                    Persona._gravedad_base = 0.15
                elif Persona.edad > 15 and Persona.edad <= 35:
                    Persona._gravedad_base = 0.25
                elif Persona.edad > 35 and Persona.edad <= 60:
                    Persona._gravedad_base = 0.40
                elif Persona.edad > 60 and Persona.edad <= 90:
                    Persona._gravedad_base = 0.50
                elif Persona.edad > 90:
                    Persona._gravedad_base = 0.7
                # Enfermedad_ base a todos las personas
                # Afeccion a todas las personas
                # Sin Vacuna a todas las personas
                Persona.enfermedad_base = Ninguna_Enfermedad()
                Persona.afeccion = Ninguna_Afeccion()
                Persona.vacuna = vacuna.Sin_Vacuna()
                # Agregar Ciudadanos en la comunidad
                self._comunidad_ciudadanos.append(Persona)
            self._Promedio = sumatoria_edades / self._num_ciudadanos

            # Famila
            lista_aleatoria = self._comunidad_ciudadanos
            r.shuffle(lista_aleatoria)
            lista = []
            for individuo in lista_aleatoria:
                lista.append(individuo)
                if len(lista) == 5:
                    for individuo in lista_aleatoria:
                        if individuo in lista:
                            for familiar in lista:
                                if individuo != familiar:
                                    individuo.familia(familiar)
                    lista = []

            # Amigos
            for integrante in self._comunidad_ciudadanos:
                integrante.amigos()
            # Enfermedades Base
            numero_con_enfermedades = int(round(self._num_ciudadanos * 0.25))
            self._total_enfermos = numero_con_enfermedades
            enfermos_base = r.sample(self._comunidad_ciudadanos, numero_con_enfermedades)
            for paciente in enfermos_base:
                paciente.enfermedad_base = r.choice(self._enfermedades)
            # Afecciones o Condiciones
            numero_con_afecciones = int(round(self._num_ciudadanos * 0.65))
            self._total_afecciones = numero_con_afecciones
            afectados = r.sample(self._comunidad_ciudadanos, numero_con_afecciones)
            for paciente in afectados:
                paciente.afeccion = r.choice(self._afecciones)

            # Enfermedades y Afecciones
            interseccion = []
            resultado = [n1 for n2 in enfermos_base for n1 in afectados if n1 == n2 and n1 not in interseccion]
            self._total_afecciones_y_enfermos = len(resultado)
