### Proyecto 1 Mejoramiento - Programación Avanzada
- Se crea un programa computacional que aplique el paradigma de programación orientada a objetos en python en la simulación de la propagación de una enfemedad infecciosa en una comunidad estableciada basandose en el modelo SIR.  Con un cambio, la manipulación de nuevas condiciones o objetos como enfermedades bases, afeccion y vacunas. 
- Las enfermedades bases y afeccion son objetos aumentan la probabilidad de entrar en estado grave en situación de ser infectado, al llegar el caso de muerte ante la enfermedad infecciosa.
- Las vacunas son objetos que disminuyen la posibilidad de entrar en estado grave y prevenir la muerte ante la enfermedad infecciosa.

### Integrantes 
- Bastián Morales 

### Modelo SIR [[1]](http://mat.uab.cat/matmat/PDFv2013/v2013n03.pdf)
El modelo SIR describe la dinámica del sistema: Cada persona infectada puede infectar a cualquier persona susceptible (independientemente) con probabilidad Β y puede recuperarse con probabilidad Γ. Dado que S(t), I(t) y R(t) denotan el número de personas susceptibles, infectadas y recuperadas en el momento t, respectivamente.
- Población susceptible (S), individuos sin inmunidad al agente infeccioso, y que por tanto puede ser infectada si es expuesta al agente infeccioso.

- Población infectada (I), individuos que están infectados en un momento dado y pueden transmitir la infección a individuos de la población susceptible con la que entran en contacto.

- Población recuperada y fallecidos (R), individuos que son inmunes a la infección (o fallecidos), y consecuentemente no afectan a la transmisión cuando entran en contacto con otros individuos.

Objetos
========
-  Se presentan clases  previamente definidas con el fin de crear objetos, que interactuaran entre sí para llevar a cabo la simulación, de forma correcta y controlada en su funcionamiento.

### Enfermedades Bases
- En nuestra simulacion, corresponde a las enfermedades base , es decir, enfermedades que nace cada individuo que pueden ser  entre la enfermedad Cerebro Vascular, Enfermedad Fibrosis Quistica, Enfermedad Asma o Enfermedad Hipertension respectivamente. Cada Enfermedad anteriormente mencionadas tienen como atributos, su nombre y la probabilidad de gravedad ante la situación de ser infectada por la enfermedad infecciosa presente en la simulación.

###  Afecciones o Condición
- En nuestra simulación, corresponden como condición o afeccion que nacen un individuo entre ellas especialmente Obesidad o Desnutrición. Nuevamente tienen como atributos, su nombre y la probabilidad de gravedad ante la situación de ser infectada por la enfermedad infecciosa presente en la simulación.

### Vacuna
- En nuestra simulacion, corresponden a objetos que ayudan a prevenir la posibilidad de entrar en estado grave en caso de ser infectado, al mismo tiempo la muerte del ciudadano correspondiente.

### Enfermedad
- En nuestra simulación, corresponde a la enfermedad que se luchara en la comunidad. Que tiene como atributos una cierta probabilidad de infección y un promedio de pasos en los cuales un ciudadano se recupera o muera.

### Ciudadano
- Corresponde a un  ciudadano que pertenece a una comunidad simulada y los principales atributos que tiene: comunidad que pertenece, grupo de amigos que es una lista de objetos tipo ciudadanos, Id único, estado de salud, enfermedad base, afeccion, edad y la enfermedad al contagiarse.

### Comunidad
- Representa la comunidad tiene como objetivo fundamental describir a un grupo de ciudadanos, enfermedad especifica y las conexiones entre los ciudadanos relevantes para la simulación.


### Simulador 
- Corresponde a la clase en la cual se encargará de inicializar y llevar el control de los pasos en la simulación, con los diferentes objetos previamente mencionados. Se determina la comunidad y se crean los pacientes infectados iniciales, que son personas al azar a la cual se infecta, para que la enfermedad presente se empiece a propagarse a traves de los ciudadanos. Se distribuyen las vacunas disponibles entre la comunidad. Analizando cada dia el estado de cada ciudadano como: infectado, graves, recuperados, muertos y vacunados.


### Menu  Principal
- Corresponde a la clase en la cual se inicializa todo lo necesario para la simulación de la enfermedad en una comunidad  y esta es llevada a cabo.