class Vacuna():
    "Clase de Objeto Tipo Vacuna - Clase Padre"

    def _init(self):
        self._probabilidad_immunidad = None
        self._num_dosis = 0
        self._num_pasos = 0
        self._id_vacuna = 0

    @property
    def nombre(self):
        return self._nombre

    @property
    def num_dosis(self):
        return self._num_dosis

    @property
    def pasos_entre_dosis(self):
        return self._num_pasos

    @property
    def id_vacuna(self):
        return self._id_vacuna

    @property
    def probabilidad_inmunidad(self):
        return self._probabilidad_immunidad

"Clase de Objetivo Tipo Vacuna 1 - Clase Hija"
class Vacuna_1(Vacuna):
    def __init__(self):
        super().__init__()
        self._probabilidad_immunidad = 0.25
        self._num_dosis = 2
        self._num_pasos = 3
        self._id_vacuna = 1

"Clase de Objetivo Tipo Vacuna 2 - Clase Hija"
class Vacuna_2(Vacuna):
    def __init__(self):
        super().__init__()
        self._probabilidad_immunidad = 0.50
        self._num_dosis = 2
        self._num_pasos = 6
        self._id_vacuna = 2

"Clase de Objetivo Tipo Vacuna 3 - Clase Hija"
class Vacuna_3(Vacuna):
    def __init__(self):
        super().__init__()
        self._probabilidad_immunidad = 1
        self._num_dosis = 1
        self._num_pasos = 0
        self._id_vacuna = 3
"Clase de Objetivo Tipo Vacuna Sin Vacuna - Clase Hija"
class Sin_Vacuna(Vacuna):
    def __init__(self):
        super().__init__()
        self._probabilidad_immunidad = 0
        self._num_dosis = 0
        self._num_pasos = 0
        self._id_vacuna = 0
