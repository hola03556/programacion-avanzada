import pickletools
from comunidad import Comunidad
from ciudadano import Ciudadano
from enfermedad import Enfermedad
import random as rd
import vacuna
from time import sleep


def reporte_inicial(total_poblacion, total_enfermedades, total_afecciones, total_vacunas, total_enfermos_y_afectados, Promedio):

    print("\n\n\n##########################################################")
    print(f'#                    Reporte Inicial                     #')
    print("##########################################################\n\n\n")

    print("*-----------------------------------------------------------------------------------------------------*")
    print(f'★  Total de poblacion: {total_poblacion}')
    print(f'★  Total de poblacion con: Afeccion - {total_afecciones} * Enfermedades Base - {total_enfermedades} * Ambas - {total_enfermos_y_afectados}')
    print(f'★  Promedio de edad de la comunidad: {Promedio}')
    print(f'★  Vacunas disponibles: {int(total_vacunas)}')
    print("*-----------------------------------------------------------------------------------------------------*")

def reporte_final(total_poblacion, contador_afecciones, contador_enfermos_base, contador_afecciones_y_enfermos, num_inoculados, recuperados, muertos, promedio, infectados_actuales ):


    print("\n\n\n##########################################################")
    print(f'#                     Reporte Final                     #')
    print("##########################################################")

    print("*-----------------------------------------------------------------------------------------------------*")
    print(f'☆  Total de poblacion: {total_poblacion}')
    print(f'☆  Total de poblacion con: Afeccion - {contador_afecciones} * Enfermedades Base - {contador_enfermos_base} * Ambas - {contador_afecciones_y_enfermos}')
    print(f'☆  Total de contagiados: {infectados_actuales}')
    print(f'☆  Total de Inmunes o Recuperados: {recuperados}')
    print(f'☆  Total de fallecidos: {len(muertos)}')
    print(f'☆  Promedio de edad de la comunidad: {promedio}')
    print(f'☆  Numero de Inoculados: {int(num_inoculados)}')
    print("*-----------------------------------------------------------------------------------------------------*")

class Simulador():
    def __init__(self, comunidad):
        self._comunidad = comunidad
        self._vacuna_1 = vacuna.Vacuna_1()
        self._vacuna_2 = vacuna.Vacuna_2()
        self._vacuna_3 = vacuna.Vacuna_3()
        self._todos = 0
        self._pasos = 0

    # Crear el Objeto Comunidad 
    def set_comunidad(self):
        # Infectados
        infectados = rd.sample(self._comunidad._comunidad_ciudadanos, self._comunidad._num_infectados)
        for inf in infectados:
            inf.enfermedad = self._comunidad.enfermedad
            inf.dias_cuarentena = self._comunidad.enfermedad.promedio_pasos
            inf.seriedad()
            inf.condicion()


    def run(self, pasos):
        recuperados  = 0
        muertos = []
        peak = 0
        # Reporte Inicial
        reporte_inicial(total_poblacion = self._comunidad._num_ciudadanos,total_enfermedades = self._comunidad._total_enfermos,
                        total_afecciones = self._comunidad._total_afecciones, total_vacunas= self._comunidad._total_vacunas, 
                        total_enfermos_y_afectados = self._comunidad._total_afecciones_y_enfermos, Promedio = self._comunidad._Promedio)

        print("------------------------------------------------------------------")
        print(f'Dia *0*: Susceptibles {len(self._comunidad._comunidad_ciudadanos)} - Recuperados 0 - Infectados {self._comunidad._num_infectados}')
        # Inicia la simulucion por dias 
        for dias in range(pasos):
            
            if dias >= 10 and self._todos != 1:
                vacunados = rd.sample(self._comunidad._comunidad_ciudadanos, int(self._comunidad._total_vacunas))
                contador_vac_1 = 0
                contador_vac_2 = 0

                for paciente in vacunados:
                    if contador_vac_1 < self._comunidad._total_vacunas_1:
                        paciente.vacuna = self._vacuna_1
                        contador_vac_1 += 1
                    elif contador_vac_2 < self._comunidad._total_vacunas_2:
                        paciente.vacuna = self._vacuna_2
                        contador_vac_2 += 1
                    else:
                        paciente.vacuna = self._vacuna_3
                    paciente.dosis()
                    paciente.primera_dosis_dia(dias)
                self._todos = 1
            
            if self._todos == 1:
                for paciente in vacunados:
                    dia_segunda_dosis = paciente._primera_dosis + paciente.vacuna._num_pasos
                    if dias == dia_segunda_dosis and paciente.vacuna._id_vacuna == 1:
                        paciente.dosis()
                    if dias == dia_segunda_dosis and paciente.vacuna._id_vacuna == 2:
                        paciente.dosis()

            
            infectados_actuales = 0
            graves = 0
            print("------------------------------------------------------------------")
            # Se recorre cada persona de la comunidad
            for persona in self._comunidad._comunidad_ciudadanos:
                # Si la persona esta sana pasa a la siguiente persona
                # Muertos
                if persona._severidad is True and persona._muerto is True and persona._entierro_inmediato is True and persona not in muertos:
                    muertos.append(persona)
                    continue

                if not persona.estado and persona._muerto is False:
                    # Si la persona está contagiada, se le va quitando dias de su cuarentena
                    persona.dia_siguiente()
                    persona.seriedad()
                    # Si la persona completa su cuarentena, se recupera y ya no es capaz de contagiar a más personas
                    # Pasa a la siguiente persona 
                    if persona.inmunidad:
                        persona.condicion()
                        # Se suma al total de recuperados
                        recuperados += 1
                        continue
                    # Se acumula las personas contagiadas del dia o actualmente
                    infectados_actuales += 1
                    # Se ve la probabilidad si hay reunión con una persona o no
                    reunion = rd.randint(0,100)
                    # Graves 
                    if persona._severidad is True and persona._muerto is False:
                        persona.dia_siguiente()
                        graves += 1
                        persona.muerto_o_sano()
                        continue
                    
                    if reunion <= self._comunidad.probabilidad_conexion_fisica * 100:
                        # Si hay reunión, se escoge una persona aleatoria de su grupo de amigos
                        encuentro = rd.choice(persona.grupo)
                        # Si la persona escogida esta sana y sin inmunidad a la enfermedad presente
                        if encuentro.estado and not encuentro.inmunidad and encuentro.vacuna._id_vacuna != 3:
                            # probabilidad si se contagie de la enfermedad
                            contagio = rd.randint(0,100)
                            # La persona esta por contagiarse
                            if contagio <= self._comunidad.enfermedad.infeccion_problable * 100:
                                # La persona se contagia con la enfermedad
                                encuentro.enfermedad = self._comunidad.enfermedad
                                # SE le ingresa la cantidad de dias que tiene que hacer cuarentena
                                encuentro.dias_cuarentena = self._comunidad.enfermedad.promedio_pasos
                                # La persona cambia de estado no sano
                                encuentro.condicion()
            if peak == 0 or peak < infectados_actuales:
                peak = infectados_actuales
                dia = dias
            # Total de susceptibles dependiente a la cantidad de recuperados y infectados
            susceptibles = len(self._comunidad._comunidad_ciudadanos) - recuperados - infectados_actuales - len(muertos)
            # Reporte de susceptibles, infectados y recuperados por cada dia
            print(f'Dia *{dias + 1}*: Susceptibles {susceptibles} - Recuperados {recuperados} - Infectados {infectados_actuales} - Graves {graves} - Muertos {len(muertos)}')
            #sleep(0.5)
        print("\n\n##########################################################")
        print(f'# El peak de infectados fue de {peak} -- El dia {dia+1} #')
        print("##########################################################")


        contador_enfermos_base = 0
        contador_afecciones = 0
        contador_afecciones_y_enfermos = 0
        sumatoria = 0
        num_inoculados = 0
        for sobreviviente in self._comunidad._comunidad_ciudadanos:
            if sobreviviente._muerto is False:
                if sobreviviente.enfermedad_base._probabilidad_gravedad != 0 and sobreviviente.afeccion._probabilidad_gravedad == 0:
                    contador_enfermos_base +=1
                elif sobreviviente.enfermedad_base._probabilidad_gravedad == 0 and sobreviviente.afeccion._probabilidad_gravedad != 0:
                    contador_afecciones += 1
                elif sobreviviente.enfermedad_base._probabilidad_gravedad != 0 and sobreviviente.afeccion._probabilidad_gravedad != 0:
                    contador_afecciones_y_enfermos += 1
                sumatoria += sobreviviente.edad
                if sobreviviente.vacuna._id_vacuna != 0:
                    num_inoculados += 1
        promedio = sumatoria / self._comunidad._num_ciudadanos
        total_poblacion = len(self._comunidad._comunidad_ciudadanos) - len(muertos)

        reporte_final(total_poblacion, contador_afecciones, contador_enfermos_base, contador_afecciones_y_enfermos, num_inoculados, recuperados, muertos, promedio, infectados_actuales )