class Afeccion():
    "Clase de Objeto Tipo Afeccion  - Clase Padre"
    def __init__(self):
        self._nombre = ""
        self._probabilidad_gravedad = None

    @property
    def nombre(self):
        return self._nombre

    @property
    def probabilidad_gravedad(self):
        return self._probabilidad_gravedad

"Clase de Objeto Tipo Afeccion - Obesidad  - Clase Hija"
class Obesidad(Afeccion):
    def __init__(self):
        super().__init__()
        self._nombre = "Obesidad"
        self._probabilidad_gravedad = 0.5

"Clase de Objeto Tipo Afeccion - Desnutricion  - Clase Hija"
class Desnutricion(Afeccion):
    def __init__(self):
        super().__init__()
        self._nombre = "Desnutrición"
        self._probabilidad_gravedad = 0.2

"Clase de Objeto Tipo Afeccion - Ninguna Afeccion  - Clase Hija"
class Ninguna_Afeccion(Afeccion):
    def __init__(self):
        super().__init__()
        self._nombre = "Sin Afección"
        self._probabilidad_gravedad = 0

