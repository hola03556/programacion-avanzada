from vacuna import Vacuna
from enfermedad_base import Enfermedad_Base
from afeccion import Afeccion
from enfermedad import Enfermedad
import random as r

class Ciudadano():
    # Clase del objeto Ciudadano 
    def __init__(self, comunidad):
        self._comunidad = comunidad
        self._id_ciudadano = ''
        self._grupo= []
        self._enfermedad = None
        self._estado = True
        self._inmunidad = False
        self._dias_cuarentena = None
        self._edad = 0
        self._vacuna = None
        self._dosis_presentes = 0
        self._primera_dosis = 0
        self._enfermedad_base = None
        self._afeccion = None
        self._gravedad_base = 0
        self._severidad = False
        self._muerto = False
        self._entierro_inmediato = False
        self._defecto = 1


    @property
    def comunidad(self):
        return self._comunidad

    @property
    def id_ciudadano(self):
        return self._id_ciudadano

    @id_ciudadano.setter
    def id_ciudadano(self, codigo):
        if isinstance(codigo,str):
            # Se crea el ID unico de cada ciudadano
            cantidad_total = str(self._comunidad._num_ciudadanos)
            if len(cantidad_total) > len(codigo):
                texto_id = '0'*(len(cantidad_total)-len(codigo))
                codigo_texto = f'{texto_id}{codigo}'
                self._id_ciudadano = codigo_texto
            else:
                codigo_texto = f'{codigo}'
                self._id_ciudadano = codigo_texto

    @property
    def grupo(self):
        return self._grupo

    def familia(self,familiar):
        self._grupo.append(familiar)

    def amigos(self):
        grupo_total = self._comunidad._promedio_conexion_fisica + len(self._grupo)
        # Se crea el grupo de amigos de cada ciudadano 
        while len(self._grupo) < grupo_total:
            amigo = r.choice(self._comunidad.comunidad_ciudadanos)
            # Sin que se repita un amigo o salgo uno mismo
            if (amigo not in self._grupo) and (amigo._id_ciudadano != self._id_ciudadano):
                self._grupo.append(amigo)

    @property
    def enfermedad(self):
        return self._enfermedad
    
    @enfermedad.setter
    def enfermedad(self,virus):
        # Se le asigna la enfermedad a una persona
        if isinstance(virus,Enfermedad):
            self._enfermedad = virus

    @property
    def estado(self):
        return self._estado
    
    def condicion(self):
        # Si esta sano o infectado el ciudadano
        self._estado = not self._estado

    @property
    def dias_cuarentena(self):
        return self._dias_cuarentena

    @dias_cuarentena.setter
    def dias_cuarentena(self, pasos):
        # Los dias de cuarentena que debe cumplir si se infecta un ciudadano
        if isinstance(pasos,int):
            self._dias_cuarentena = pasos
    
    def dia_siguiente(self):
        # Se le resta un dia a la cuarentena hasta que termine 
        if self._dias_cuarentena > 0:
            self._dias_cuarentena -= 1
        # Cuando termina la cuarentena, el ciudadano esta sano o muerto, sin que se vuelva a contagiar
        else:
            self._inmunidad = True

    @property
    def inmunidad(self):
        return self._inmunidad

    @property
    def edad(self):
        return self._edad 

    @edad.setter
    def edad_(self, anio):
        if isinstance(anio,int):
            self._edad = anio

    @property
    def vacuna(self):
        return self._vacuna 

    @vacuna.setter
    def vacuna(self, inyeccion):
        # Se le asigna una vacuna a una persona
        if isinstance(inyeccion, Vacuna):
            self._vacuna = inyeccion
    
    def seriedad(self):
        # La gravedad o severidad del ciudadano ante la enfermedad  estando contagiado o no
        potenciador = self._defecto +  self._afeccion.probabilidad_gravedad + self._enfermedad_base.probabilidad_gravedad
        peligrosidad = self._gravedad_base * potenciador
        disminucion = self._vacuna._probabilidad_immunidad/2 * self._dosis_presentes
        seriedad = peligrosidad - disminucion
        if self._vacuna._id_vacuna == 3:
            seriedad = 0
            self._severidad = False
            self._inmunidad = True
            self._estado = not self._estado
        if seriedad >= 0.6:
            self._severidad = True
        else:
            self._severidad = False

    @property
    def enfermedad_base(self):
        return self._enfermedad_base

    @enfermedad_base.setter
    def enfermedad_base(self, enfermedad_inicial):
        # Se le asigna la enfermedad base a una persona
        if isinstance(enfermedad_inicial, Enfermedad_Base):
            self._enfermedad_base = enfermedad_inicial

    @property
    def afeccion(self):
        return self._afeccion

    @afeccion.setter
    def afeccion(self, condicion):
        # Se le asigna la afeccion a una persona
        if isinstance(condicion,Afeccion):
            self._afeccion = condicion
    

    @property
    def gravedad(self):
        return self._gravedad_base

    @gravedad.setter
    def gravedad(self, riesgo):
        if isinstance(riesgo, float):
            self._gravedad_base = riesgo

    def dosis(self):
        # Se le asigna la siguiente dosis a una persona que fue vacunada anteriormente
        if self._vacuna.id_vacuna == 1 and self._dosis_presentes < self._vacuna.num_dosis:
            self._dosis_presentes += 1
        elif self._vacuna.id_vacuna == 2 and self._dosis_presentes < self._vacuna.num_dosis:
            self._dosis_presentes += 1
        elif self._vacuna.id_vacuna == 3:
            self._dosis_presentes = 0
    
    def primera_dosis_dia(self, dia):
        self._primera_dosis = dia

    def muerto_o_sano(self):
        # Se le asigna probabilidad si la persona muere o no, dependiendo si esta enferma con el virus, esta estado severo y no entierro inmediato
        muerto_o_sano = (r.randint(1,100))/100
        if ((self._estado is False) and (self._severidad is True) and (muerto_o_sano <= 0.4) and (self._entierro_inmediato is False)):
            self._muerto = True
            self._entierro_inmediato = not self._entierro_inmediato
