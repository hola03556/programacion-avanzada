class Enfermedad_Base():
    "Clase de Objeto Tipo Enfermedad Base - Clase Padre"
    def __init__(self):
        self._nombre = ""
        self._probabilidad_gravedad = None

    @property
    def nombre(self):
        return self._nombre

    @property
    def probabilidad_gravedad(self):
        return self._probabilidad_gravedad

"Clase de Objeto Tipo Enfermedad Base - Asma - Clase Hija"
class Asma(Enfermedad_Base):
    def __init__(self):
        super().__init__()
        self._nombre = "Asma"
        self._probabilidad_gravedad = 0.75

"Clase de Objeto Tipo Enfermedad Base - Cerebro Vascular - Clase Hija"
class Cerebro_vascular(Enfermedad_Base):
    def __init__(self):
        super().__init__()
        self._nombre = "Cerebro Vascular"
        self._probabilidad_gravedad = 0.55

"Clase de Objeto Tipo Enfermedad Base - Fibrosis Quistica - Clase Hija"
class Fibrosis_Quistica(Enfermedad_Base):
    def __init__(self):
        super().__init__()
        self._nombre = "Fibrosis Quistica"
        self._probabilidad_gravedad = 0.5

"Clase de Objeto Tipo Enfermedad Base - Hipertension - Clase Hija"
class Hipertension(Enfermedad_Base):
    def __init__(self):
        super().__init__()
        self._nombre = "Hipertensión"
        self._probabilidad_gravedad = 0.35
    
"Clase de Objeto Tipo Enfermedad Base - Ninguna - Clase Hija"
class Ninguna_Enfermedad(Enfermedad_Base):
    def __init__(self):
        super().__init__()
        self._nombre = "Sin enfermedad"
        self._probabilidad_gravedad = 0

